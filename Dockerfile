FROM balenalib/rpi-raspbian:buster-20191127

WORKDIR /root

COPY ./demo.mp4 .
COPY ./streaming_into_v4l2.sh .

RUN apt-get update && apt-get -y install ffmpeg


CMD [ "/root/streaming_into_v4l2.sh"]
