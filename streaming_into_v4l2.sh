#!/bin/bash
#
# This script loads the required kernel module for playing a video file into the camera interface
#
# The module can be built from: https://github.com/umlaeute/v4l2loopback
#
# sudo apt-get install ffmpeg

CAMERA_DEVICE=""

function find_camera() {
        EXISTING_INTERFACES=$(ls /dev/video* 2>/dev/null)
        if [ -z "${EXISTING_INTERFACES}" ]
        then
                echo "No cameras found"
                exit -1
        fi
        CAMERA_DEVICE=$(echo "${EXISTING_INTERFACES}" | head -n 1)
        if [ ! -e "${CAMERA_DEVICE}" ]
        then
                echo "Could not open device ${CAMERA_DEVICE}" 
                exit -1
        fi
        echo "Camera ${CAMERA_DEVICE} selected"
}
        
find_camera
ffmpeg ${FFMPEG_OPTIONS} -stream_loop -1 -re -i /root/demo.mp4 -f v4l2 ${CAMERA_DEVICE}
